description = """
pre_R_script.py: Cleans up raw Seahorse input data (creates .csv copies of .xlsx input files for manipulation, renames some fields), \
performs some simple calculations (e.g. basal_OCR, spare capacity etc.) that are required for the more complex calculations \
performed by the treat_seahorse_data.r script, and programatically executes the treat_seahorse_data.r script to produce statistical summaries
that are also output to .csv files. See the README.md file for additional information on using this script and treat_seahorse_data.r script.
"""

# Import the modules
import pandas as pd 
import numpy as np
import pathlib 
import argparse
from xlsx2csv import Xlsx2csv
import subprocess
import platform
import sys
import re

# Define the functions
def filter_intact_or_perm(df, condition_to_filter_on):
    """
    Filters dataframe to one of two Seahorse conditions: where the 
    experimental 'Group' value is 'intact' or 'perm'. Filtering is 
    performed here because different calculations are performed on
    each group. 

    Takes in a dataframe and condition to filter on (str, must be
    either 'intact' or 'perm') and returns a dataframe that has been
    filtered to contain only 'intact' or 'perm' 'Group' values. 
    """
    df_copy = df.copy() # avoid pandas duplication error
    df_condition = df[df.Group.str.endswith(condition_to_filter_on)]


def calc_timepoint_filter(df, first_timepoint, last_timepoint):
    """To save on memory, filters raw Seahorse CSV data to the first
    and last timepoints required to perform each calculation (as defined
    in relevant functions).
    
    Takes in a dataframe of the 'Rate' sheet from the raw Seahorse CSV data
    and first and last timepoints (integers) as inputs, and returns a filtered
    dataframe of only rows with a 'Measurement' value between first_time, last_time
    as an output. 
    
    NOTE: 'Measurement' column (series) name in Seahorse CSV data refers to the
    timepoint at which each measurement was taken."""
    df_copy = df.copy() # avoid pandas duplication error
    df_copy['in_timepoint'] = df_copy['Measurement'].between(first_timepoint, last_timepoint, inclusive=True) # filter to timepoints of interest
    rate_sheet_timepoints = df_copy[df_copy['in_timepoint'] == True].drop('in_timepoint', axis=1) # filter to timepoints of interest
    return rate_sheet_timepoints # return filtered dataframe 


def initial_timepoint_filter(df, last_timepoint, first_timepoint=1):
    """To save on memory, performs initial filter of raw Seahorse
    CSV data between the first relevant timepoint (default value of 1;
    can be overwritten by assignment) and last_timepoint by calling
    calc_timepoint_filter().
    
    Takes in a dataframe of the 'Rate' sheet from the Seahorse raw CSV and the
    last_timepoint and first_timepoint (as integers) as inputs and returns
    a filtered dataframe as outut"""
    rate_sheet_timepoints = calc_timepoint_filter(df, first_timepoint, last_timepoint) # filter based on relevant timepoints for all subsequent calculations
    return rate_sheet_timepoints # return filtered dataframe 


def calc_non_mito_OCR(df, first_time, last_time):
    """Filters dataframe to timepoints required to perform calculation by
    calling calc_timepoint_filter(), calculates non-mitochondrial OCR in a
    new dataframe by averaging values for each well per timepoint and merges 
    this dataframe with the input dataframe (df) based on 'Well' value.
    
    Takes in a dataframe of the 'Rate' sheet from the raw Seahorse CSV output
    and first and last timepoints (integers) as inputs and outputs a merged 
    dataframe containing a new 'non_mito_OCR' column (series)"""
    non_mito_OCR = calc_timepoint_filter(df, first_time, last_time) # filter based on relevant timepoints for non_mito_OCR calculation
    non_mito_OCR_pivot = non_mito_OCR.groupby(['Well'])['OCR'].mean().reset_index() # temporary pivot table to group by 'Well' (e.g A1) and calculate average of timepoints for that well 
    non_mito_OCR_pivot.rename(columns = {'OCR':'non_mito_OCR'}, inplace = True) # by default the new 'mean' column will take on the name of the column the calculation it was 
    # performed on ('OCR'). Rename to non_mito_OCR
    non_mito_OCR_df = pd.merge(df, non_mito_OCR_pivot, on='Well') # Perform merge between input dataframe and non_mito_OCR_pivot on 'Well' value
    return non_mito_OCR_df # return merged dataframe containing a new 'non_mito_OCR' column (series)


## INTACT-specific calculation functions
def basal_maximal_OCR(df, first_time, last_time, type_OCR): 
    """Filters dataframe to timepoints required to perform calculation by
    calling calc_timepoint_filter(), calculates basal_OCR/maximal_OCR by
    (basal/maximal OCR - non_mito_OCR) and maps (creates or renames) additional 
    columns (series; Group, Days on Drug, Reading) required for final R script.
    
    Takes in non_mito_OCR_df returned by calling calc_non_mito_OCR(), first and 
    last timepoints (integers), and type_OCR as inputs, where type_OCR can have
    a value of 'Basal' or 'Maximal' and determines which timepoints the calculation
    is applied to (as it is the same calculation performed on the lower and upper
    range timepoints, respectively)

    NOTE: Requires the Group names to follow a strict naming convention of:
    X day treatment; where X is number of days on drug and treatment is the
    treatment condition. E.g. '0 day drug' or '1 day clinda'. 
    """
    type_OCR = type_OCR.title() # type of calculation to perform; basal or maximal. Convert to title case 
    calc_OCR = calc_timepoint_filter(df, first_time, last_time) # filter on relevant timepoints for each calculation
    calc_OCR[f'{type_OCR}_mOCR'] = calc_OCR['OCR'] - calc_OCR['non_mito_OCR'] # calculate basal/maximal. Same calculation, different timepoints. 
    
    # Control flow; define 'Reading' value for np.select() filter based on Basal/Maximal (and corresponding timepoints ('Measurements'))
    conditions = []
    values = []
    for i in range(int(first_time), int((last_time) + 1)):
        conditions.append((calc_OCR['Measurement'] == i))
        values.append(f'{type_OCR} mOCR{i}')

    calc_OCR['Reading'] = np.select(conditions, values) # create new column (series) 'Reading' based on conditions:values
    calc_OCR = calc_OCR[calc_OCR.Group.str.endswith('intact')] # filter on events of interest, which end with 'intact'
    calc_OCR['Days_on_Drug'] = calc_OCR['Group'].apply(lambda x: x.split()[0]) # Create new column for R script input 
    calc_OCR = calc_OCR[['Measurement', 'Well', 'Group', 'Days_on_Drug', 'Reading', f'{type_OCR}_mOCR']] # Filter only on columns needed for input to final R script

    return calc_OCR # return dataframe containing either basal/max OCR data only
    

def calc_spare_capacity(maximal_OCR_out, basal_OCR_out, first_time, last_time):
    """Filters dataframe to timepoints required to perform calculation by
    calling calc_timepoint_filter(), merges maximal_OCR and basal_OCR dataframes
    and then calculates spare capacity by (maximal OCR - basal OCR).

    Takes in maximal_OCR_out and basal_OCR_out dataframes that are returned by calling 
    basal_maximal_OCR calc_non_mito_OCR() twice (once each for basal and maximal) as inputs
    and returns a dataframe of spare_capacity data as the output."""
    spare_capacity = pd.DataFrame(np.hstack([maximal_OCR_out,basal_OCR_out])) # hack to merge maximal_OCR and basal_OCR dataframes needed for calculation
    # resulting df 'spare_capacity' has column (series) names replaced with numbered index
    spare_capacity = spare_capacity[[0, 1, 2, 5, 9, 11]] # filter on desired columns (series) via index
    spare_capacity.rename(columns = {0:'Measurement', 1:'Well', 2:'Group', 5:'Maximal mOCR', 9:'Days_on_Drug', 11:'Basal mOCR'}, inplace = True) # remap index numbers to readable column (series) names
    spare_capacity['OCR'] = spare_capacity['Maximal mOCR'] - spare_capacity['Basal mOCR'] # Calculate spare capacity. Column (series) required to be called 'OCR' for input into R script

    conditions = []
    values = []
    for i in range(int(first_time), int((last_time) + 1)):
        conditions.append((spare_capacity['Measurement'] == i))
        values.append(f'Spare capacity {i}')

    spare_capacity['Reading'] = np.select(conditions, values) # create new column (series) 'Reading' based on conditions:values
    spare_capacity = spare_capacity[['Well', 'Group', 'Days_on_Drug', 'Reading', 'OCR']] # filter on columns required for input to final R script

    return spare_capacity # return dataframe containing spare_capacity data only


def calc_basal_ECAR(df, first_time, last_time):
    """Filters dataframe to timepoints required to perform calculation by
    calling calc_timepoint_filter(), filters on 'intact' experimental group
     and returns first n values, where n is the number of measurements (e.g. 3 values if n=3)."""
    basal_ECAR = calc_timepoint_filter(df, first_time, last_time) # filter on relevant timepoints for each calculation
    basal_ECAR = basal_ECAR[basal_ECAR.Group.str.endswith('intact')] # filter on events of interest, which end with 'intact' 
    basal_ECAR['Days_on_Drug'] = basal_ECAR['Group'].apply(lambda x: x.split()[0]) # create new column (series) 'Group' required for input into final R script 

    conditions = []
    values = []
    for i in range(int(first_time), int((last_time) + 1)):
        conditions.append((basal_ECAR['Measurement'] == i))
        values.append(f'Basal ECAR {i}')

    basal_ECAR['Reading'] = np.select(conditions, values) # create new column (series) 'Reading' based on conditions:values
    basal_ECAR = basal_ECAR[['Well', 'Group', 'Days_on_Drug', 'Reading', 'ECAR']] # filter on columns required for input to final R script
    basal_ECAR.rename(columns = {'ECAR':'OCR'}, inplace = True) # standardise column name for input into final R script
    
    return basal_ECAR


## PERM-specific calculation functions
def substrate_or_tmpd_dependent_mOCR(df, first_time, last_time, type_OCR):
    """Filters dataframe to timepoints required to perform calculation by
    calling calc_timepoint_filter(), calculates basal_OCR/maximal_OCR by
    (basal/maximal OCR - non_mito_OCR) and maps (creates or renames) additional 
    columns (series; Group, Days on Drug, Reading) required for final R script.
    
    Takes in non_mito_OCR_df returned by calling calc_non_mito_OCR(), first and 
    last timepoints (integers) and type_OCR as inputs, where type_OCR can have
    a value of 'substrate' or 'tmpd' and determines which timepoints the calculation
    is applied to.
    
    Returns a dataframe with new 'substract_dependent_mOCR'
    column as an output.

    NOTE: Requires the Group names to follow a strict naming convention of:
    X day treatment; where X is number of days on drug and treatment is the
    treatment condition. E.g. '0 day drug' or '1 day clinda'. 
    """
    type_OCR = type_OCR.title() # type of calculation to perform; substrate-dependent or TMPD-dependent. Convert to title case 
    sub_dep_OCR = calc_timepoint_filter(df, first_time, last_time) # filter on relevant timepoints for each calculation
    sub_dep_OCR[f'{type_OCR}_dependent_mOCR'] = sub_dep_OCR['OCR'] - sub_dep_OCR['non_mito_OCR'] # calculate basal/maximal. Same calculation, different timepoints. 
    conditions = []
    values = []
    for i in range(int(first_time), int((last_time) + 1)):
        conditions.append((sub_dep_OCR['Measurement'] == i))
        values.append(f'{type_OCR}_dependent_mOCR{i}')

    sub_dep_OCR['Reading'] = np.select(conditions, values) # create new column (series) 'Reading' based on conditions:values
    sub_dep_OCR['Days_on_Drug'] = sub_dep_OCR['Group'].apply(lambda x: x.split()[0]) # Create new column for R script input
    sub_dep_OCR = sub_dep_OCR[['Measurement', 'Well', 'Group', 'Days_on_Drug', 'Reading', f'{type_OCR}_dependent_mOCR']] # Filter only on columns needed for input to final R script
    return sub_dep_OCR


def ratio_TMPD_to_substrate(tmpd_dep_out, sub_dep_out, first_time, last_time):
    """
    Filters dataframe to timepoints required to perform calculation by
    calling calc_timepoint_filter(), merges tmpd_dep_out and sub_dep_out dataframes
    and then calculates the ratio of TMPD/substrate by (tmpd_dep_out / sub_dep_out).

    Takes in tmpd_dep_out and sub_dep_out dataframes that are returned by calling 
    substrate_or_tmpd_dependent_mOCR twice (once each for TMPD and substrate) as inputs
    and returns a dataframe of ratio_TMPD_to_substrate data as the output.
    """
    ratio_tmpd_sub = pd.DataFrame(np.hstack([tmpd_dep_out,sub_dep_out])) # hack to merge tmpd_dep_out and sub_dep_OCR dataframes needed for calculation
    # resulting df 'ratio_tmpd_sub' has column (series) names replaced with numbered index
    ratio_tmpd_sub = ratio_tmpd_sub[[0, 1, 2, 5, 9, 11]] # filter on desired columns (series) via index
    ratio_tmpd_sub.rename(columns = {0:'Measurement', 1:'Well', 2:'Group', 5:'Tmpd_dependent_mOCR', 9:'Days_on_Drug', 11:'Substrate_dependent_mOCR'}, inplace = True) # remap index numbers to readable column (series) names
    ratio_tmpd_sub = ratio_tmpd_sub[(ratio_tmpd_sub['Tmpd_dependent_mOCR'] >= 1) & (ratio_tmpd_sub['Substrate_dependent_mOCR'] >= 1)] # Drop events where *mOCR less than 1 (skews results)
    ratio_tmpd_sub['OCR'] = ratio_tmpd_sub['Tmpd_dependent_mOCR']/ratio_tmpd_sub['Substrate_dependent_mOCR'] # Calculate spare capacity. Column (series) required to be called 'OCR' for input into R script
    
    conditions = []
    values = []
    for i in range(int(first_time), int((last_time) + 1)):
        conditions.append((ratio_tmpd_sub['Measurement'] == i))
        values.append(f'ratio_tmpd_substrate_mOCR {i}')

    ratio_tmpd_sub['Reading'] = np.select(conditions, values) # create new column (series) 'Reading' based on conditions:values
    ratio_tmpd_sub = ratio_tmpd_sub[['Well', 'Group', 'Days_on_Drug', 'Reading', 'OCR']] # filter on columns required for input to final R script

    return ratio_tmpd_sub # return dataframe containing spare_capacity data only


## Generic helper functions
def append_to_csv(df, out_file, count):
    """Output input dataframe to a CSV at path out_file
    in append mode using " " as the delimeter. Count is a global variable that is 
    used so that the dataframe column (series) names are
    only written to the out_file once."""
    if count == 1:
        df.to_csv(out_file, mode='a', header=True, index=False, sep=" ") # write to CSV in append mode
    else:
        df.to_csv(out_file, mode='a', header=False, index=False, sep=" ") # write to CSV in append mode


def convert_xlsx_to_csv(input_path, output_path):
    """Converts .xlsx files to .csv files. Required as the Python
    Pandas modules sometimes encounters errors handling .xlsx files
    with multiple sheets.
    
    Takes a path to an input .xlsx file and output file path  as an inputs
    and outputs a converted .csv file with " " as the delimter."""
    Xlsx2csv(input_path, outputencoding="utf-8", delimiter=" ").convert(output_path, sheetid=2) # seems to work; default csv doesn't delimit properly 


def get_rscript_path():
    """Attempts to automatically find the most recent  version of
    Rscript.exe installed. On Windows, looks in C:\\Program Files*.
    Returns a pathlib object of the latest version of Rscript.exe
    installed.
    
    Due to R normally being installed via a packager on Linux systems,
    doesn't work for Linux and assumes that R is installed, and if it is,
    will use the preferred version of R by the OS."""
    os = platform.system()
    if os ==  "Windows":
        r_dir = pathlib.Path("C:\\Program Files")
        r_script_exes = list(r_dir.rglob("*\\bin\\Rscript.exe"))
        if len(r_script_exes) == 0: # list will be empty (len 0) if it can't find Rscript.exe anywhere in the Program Files directory
            print('Script unable to identify Rscript.exe in C:\\Program Files\\. Ensure R has been installed')
            sys.exit() # exit the script, as there is no point calling the "treat_seahorse_data.r" script if R isn't installed
        else: # proceed with getting latest version of Rscript.exe; else calling -1 index on empty list returns IndexError
            r_script_exe = sorted(r_script_exes)[-1] # sort the list of Rscript executables by version number; -1 index selects the latest version
    elif os ==  "Darwin":
        r_dir = pathlib.Path("/usr/local/bin")
        r_script_exes = list(r_dir.rglob("*Rscript"))
        if len(r_script_exes) == 0: # list will be empty (len 0) if it can't find Rscript.exe anywhere in the /usr/local/bin directory
            print('Script unable to identify Rscript.exe in /usr/local/bin/. Ensure R has been installed')
            sys.exit() # exit the script, as there is no point calling the "treat_seahorse_data.r" script if R isn't installed
        else:
            r_script_exe = sorted(r_script_exes)[-1]
    elif os ==  "Linux":
        r_dir = pathlib.Path("/usr/bin")
        r_script_exes = list(r_dir.rglob("*Rscript"))
        if len(r_script_exes) == 0: # list will be empty (len 0) if it can't find Rscript.exe anywhere in the /usr/local/bin directory
            print('Script unable to identify Rscript.exe in /usr/bin/. Ensure R has been installed')
            sys.exit() # exit the script, as there is no point calling the "treat_seahorse_data.r" script if R isn't installed
        else:
            r_script_exe = sorted(r_script_exes)[-1]
    else:
        print("""Sorry, OS not currently supported. Please run on a Windows, Mac or Linux system.
        Exiting script ...""")
        sys.exit()
    return r_script_exe
    

def check_dir_empty(dir_path):
    """
    Checks if directory is empty. If empty, returns bool True. If not
    empty, returns bool False.
    """
    path = pathlib.Path(dir_path)
    has_next = next(path.iterdir(), None)
    if has_next is None:
        return True
    else:
        return False


### Main
def main():
    # argparse
    parser = argparse.ArgumentParser(description = description)
    parser.add_argument("--input", "-i", help="Directory containing raw Seahorse assay files (all repeats).", required=True)
    parser.add_argument("--output", "-o", help="Output directory to write results to. Will be created if it doesn't exist.", required=True)
    parser.add_argument("--intact", "-in", help="Perform calculations on 'intact' Group data only.", required=False, action='store_true')
    parser.add_argument("--perm", "-p", help="Perform calculations on 'perm' Group data only.", required=False, action='store_true')
    parser.add_argument("--n_measurements", "-n", help="The number of measurements to use for calculations. E.g. for three measurements, type the number 3.", required=True)
    args = parser.parse_args()
    
    # Light sanitisation of user input. Ensure correct options passed at the commandline; else exit.
    if not args.intact and not args.perm: # must specify at least one option
        print("Please specify ONE of either 'intact' or 'perm' experimental Groups to perform calculations on. Cannot specify NO options.")
        sys.exit() # exit the script
    if args.intact and args.perm: # cannot specify both options
        print("Please specify ONE of either 'intact' or 'perm' experimental Groups to perform calculations on. Cannot specify BOTH options.")
        sys.exit() # exit the script

    # Assign arguments
    input_folder = pathlib.Path(args.input)
    # Sanity check input folder exists:
    if not input_folder.exists():
        print(f'''Sorry, folder {input_folder} does not exist. Double check the folder exists and the spelling of the file path and try again.
        Exiting script ...''')
        sys.exit()
    # Sanity check if input folder exists but is empty:
    if check_dir_empty(input_folder) == True:
        print(f'''Sorry, folder {input_folder} is empty. Double check the folder has expected contents and/or the right folder was used and try again.
        Exiting script ...''')
        sys.exit()
    n_measurements = int(args.n_measurements)

    ## Initialise values for n_measurements that will be used in calculations
    # non-mitochondrial OCR, basal/maximal OCR, basal ECAR, spare capacity calculations for 'intact' data
    # non-mitochondrial OCR, substrate-dependent mOCR, FMPD-dependent mOCR, ratio-TMPD-to-substrate calculations for 'perm' data 
    initial_filter_max = n_measurements * 4 # used for initial filtering of dataframe to only required measurements
    bounds_1 = 1, n_measurements # used to calculate basal OCR for 'intact' data; not used in 'perm' calculations
    bounds_2 = (bounds_1[1] + 1), round(initial_filter_max/2) # used to calculate basal OCR for 'intact' data; calculate 'x' for 'perm' data 
    bounds_3 = (bounds_2[1] + 1), (bounds_2[1] + n_measurements) # used to calculate maximal OCR for 'intact' data; calculate 'x' for 'perm' data 
    bounds_4 = (bounds_3[1] + 1), initial_filter_max # used to calculate 'x' for 'perm' data; not used in 'intact' calculations 

    # Define output_dir and subdirectories 
    parent_dir = pathlib.Path(args.output)
    parent_dir.mkdir(parents=False, exist_ok=False)
    temp_dir = parent_dir / 'temp_dir' 
    xlsx2csv_dir = temp_dir / 'xlsx_2_csv_files' # temp dir for temporary csv files 
    csv_to_R_dir = temp_dir / 'csv_input_to_R_script' # temp dir for intermediate csv files in format for processing by R script
    intact_data_output_dir = csv_to_R_dir / 'intact_data'
    perm_data_output_dir = csv_to_R_dir / 'perm_data'
    R_script_output_dir = parent_dir / 'R_script_output' # dir for files processed by R script
    
    # Create directories
    temp_dir.mkdir(parents=False, exist_ok=False)
    xlsx2csv_dir.mkdir(parents=False, exist_ok=False)
    csv_to_R_dir.mkdir(parents=False, exist_ok=False)
    intact_data_output_dir.mkdir(parents=False, exist_ok=False)
    perm_data_output_dir.mkdir(parents=False, exist_ok=False)
    R_script_output_dir.mkdir(parents=False, exist_ok=False)

    ## Build out_file paths for each file using parent_dir as root 
    # 'Intact' data output files 
    basal_out_file = str(pathlib.Path(intact_data_output_dir) / 'basal_OCR.csv')
    maximal_out_file = str(pathlib.Path(intact_data_output_dir) / 'maximal_OCR.csv')
    spare_capacity_out_file = str(pathlib.Path(intact_data_output_dir) / 'spare_capacity_OCR.csv')
    basal_ECAR_out_file = str(pathlib.Path(intact_data_output_dir) / 'basal_ECAR_OCR.csv')
    # 'Perm' data output files 
    substrate_dep_out_file = str(pathlib.Path(perm_data_output_dir) / 'substrate_dependent_mOCR.csv')
    tmpd_dep_out_file = str(pathlib.Path(perm_data_output_dir) / 'TMPD_dependent_mOCR.csv')
    ratio_tmpd_out_file = str(pathlib.Path(perm_data_output_dir) / 'ratio_TMPD_to_substrate.csv')

    # Generate list of input files from input_dir to interate through 
    input_files = list(input_folder.glob("*.xlsx")) # only perform calculations on .xlsx files 

    print("Processing raw Seahorse .xlsx files ...")
    for file in input_files: # Iterate through and process each file in input_folder 
        temp_file = pathlib.Path(f"{str(file.name).split('.')[0]}.csv") # temp csv required for compatibility 
        # Create temporary CSV files for manipulation; preserve original files 
        temp_file_out = xlsx2csv_dir / temp_file
        convert_xlsx_to_csv(str(file), str(temp_file_out)) # convert .xlsx to csv files 

    # Use temporary CSV files for calculations
    temp_input_files = list(xlsx2csv_dir.rglob("*.csv")) # Temp files will have a .csv, not an .xlsx extension 
    count = 0 # Initialise counter used for append_to_csv function; only write column (series) names to outfile on first iteration (count=1)
    for file in temp_input_files: 
        # Iterate through input files. Create new file on first iteration; append on subsequent iterations
        count+=1 # Increment counter
        data_file = pd.read_csv(file, sep=" ") # Needs a delimeter of " " due to how Xslx2csv outputs CSV files
        file_name = str(file.stem)
        # Enforce strict file name covention of 'rep<number>_<condition>'
        if not re.match('rep[0-9]{1,2}_(intact|perm)', file_name):
            print('''Sorry, one or more of the input Seahorse files does not satisfy the naming conventions required for this script
            to work properly. The required input file naming convention is: 'rep<number>_<condition>'. For example: rep1_perm or rep1_intact.
            See 'SEAHORSE FILE NAMING REQUIREMENTS' section of the README.txt file for additional help.
            Exiting script ...
            ''')
            sys.exit()
        repeat = file_name.split("_")[0].split('rep')[1] # Extract repeat number from file name. Is used by some calculation functions. NOTE: Requires correct input file naming conventions.
        file_name_condition = file_name.split("_")[1].lower()
        # Sanity check - check experimental condition in input file names and the option passed at command line (intact or perm) match
        if file_name_condition == 'intact' and not args.intact:
            print(f"Mismatch detected: file name contains 'intact' but --intact NOT selected. Double check you selected the correct option!")
            print('Exiting script ...')
            sys.exit()
        elif file_name_condition == 'perm' and not args.perm:
            print(f"Mismatch detected: file name contains 'perm' but --perm NOT selected. Double check you selected the correct option!")
            print('Exiting script ...')
            sys.exit()

        rate_sheet = data_file[['Measurement', 'Well', 'Group', 'Time', 'OCR', 'ECAR']] # Only use columns relevant for calculations 
        rate_sheet_timepoints = initial_timepoint_filter(rate_sheet, bounds_4[-1]) # Filter dataframe only to required timepoints. Dynamically populated by user input for --n_measurements
        rate_sheet_timepoints = rate_sheet_timepoints[rate_sheet_timepoints['Group'].notna()] # Drop rows where 'Group' column is empoty 
        df_intact = rate_sheet_timepoints[rate_sheet_timepoints.Group.str.endswith('intact')] # Filter only on 'intact' data for specific calculations
        df_perm = rate_sheet_timepoints[rate_sheet_timepoints.Group.str.endswith('perm')] # Filter only on 'perm' data for specific calculations

        # Only proceed if the dataframe for the chosen experimental condition ('intact', 'perm') is NOT empty
        if args.intact and df_intact.empty:
            print('''No "intact" data was detected. Ensure that, in the values in the "Group" column of the raw Seahorse files,
                all data points in the "intact" experimental condition have "intact" at the end of the Group column value. For example, 
                if you have "0 day WT" as a "Group" column value in the "intact" condition, rename to "0 day WT intact". 
                See 'SEAHORSE FILE NAMING REQUIREMENTS' section of the README.txt file for additional help.
                Exiting script ...''')
            sys.exit()
        elif args.perm and df_perm.empty:
            print('''No "perm" data was detected. Ensure that, in the values in the "Group" column of the raw Seahorse files \
                all data points in the "perm" experimental condition have "perm" at the end of the Group column value. For example, \
                if you have "0 day WT" as a "Group" column value in the "perm" condition, rename to "0 day WT perm". \
                See 'SEAHORSE FILE NAMING REQUIREMENTS' section of the README.txt file for additional help.
                Exiting script ...''')
            sys.exit()

        ## Perform calculations on 'intact' data
        if args.intact: # Only perform if user specifies 'intact' option at commandline 
            print(f"Attempting to perform calculations on 'intact' data for {file}")
            # Calculate non_mito_OCR
            non_mito_OCR_first, non_mito_OCR_last = bounds_3[0], bounds_3[-1] # Dynamically populated by user input for --n_measurements
            non_mito_OCR_merged = calc_non_mito_OCR(df_intact, non_mito_OCR_first, non_mito_OCR_last) # Calculate non_mito_OCR by calling calc_non_mito_OCR function
            
            # Calculate basal OCR 
            basal_OCR_first, basal_OCR_last = bounds_1[0], bounds_1[-1] # Dynamically populated by user input for --n_measurements
            basal_OCR_out = basal_maximal_OCR(non_mito_OCR_merged, basal_OCR_first, basal_OCR_last, 'Basal')
            basal_OCR_out_fin = basal_OCR_out.copy()
            basal_OCR_out_fin['Measurement'] = repeat # Filter on measurements relevant to specific repeat (as defined in final name). NOTE: Requires correct input file naming conventions.
            basal_OCR_out_fin.rename(columns = {'Basal_mOCR':'OCR'}, inplace = True) # Rename column to 'OCR' as per format required for input into R script
            append_to_csv(basal_OCR_out_fin, basal_out_file, count)

            # Maximal OCR
            maximal_OCR_first, maximal_OCR_last = bounds_2[0], bounds_2[-1] # Dynamically populated by user input for --n_measurements
            maximal_OCR_out = basal_maximal_OCR(non_mito_OCR_merged, maximal_OCR_first, maximal_OCR_last, 'Maximal')
            maximal_OCR_out_fin = maximal_OCR_out.copy()
            maximal_OCR_out_fin['Measurement'] = repeat # Set value of 'Measurement' as per format required for input into R script - i.e. repeat.
            maximal_OCR_out_fin.rename(columns = {'Maximal_mOCR':'OCR'}, inplace = True) # Rename column to 'OCR' as per format required for input into R script 
            append_to_csv(maximal_OCR_out_fin, maximal_out_file, count)

            # Spare capacity
            spare_capacity = calc_spare_capacity(maximal_OCR_out, basal_OCR_out, maximal_OCR_first, maximal_OCR_last) # takes in df's from basal and maximal OCR as inputs
            spare_capacity_fin = spare_capacity.copy()
            spare_capacity_fin['Measurement'] = repeat # Filter on measurements relevant to specific repeat (as defined in final name). NOTE: Requires correct input file naming conventions.
            spare_capacity_fin = spare_capacity_fin[['Measurement', 'Well', 'Group', 'Days_on_Drug', 'Reading', 'OCR']] # Re-organise columns as per format required for input into R script
            append_to_csv(spare_capacity_fin, spare_capacity_out_file, count)

            # Basal ECAR
            basal_ECAR_first, basal_ECAR_last = bounds_1[0], bounds_1[-1] # dynamically populated by user input for --n_measurements
            basal_ECAR_out = calc_basal_ECAR(rate_sheet_timepoints, basal_ECAR_first, basal_ECAR_last)
            basal_ECAR_out_fin = basal_ECAR_out.copy()
            basal_ECAR_out_fin['Measurement'] = repeat # Filter on measurements relevant to specific repeat (as defined in final name). NOTE: Requires correct input file naming conventions.
            basal_ECAR_out_fin = basal_ECAR_out_fin[['Measurement', 'Well', 'Group', 'Days_on_Drug', 'Reading', 'OCR']] # Re-organise columns as per format required for input into R script
            append_to_csv(basal_ECAR_out_fin, basal_ECAR_out_file, count)

        ## Perform calculations on 'perm' data
        if args.perm: # Only perform if user specifies 'perm' option at commandline
            print(f"Attempting to perform calculations on 'perm' data for {file}")
            # Calculate non_mito_OCR
            non_mito_OCR_first, non_mito_OCR_last = bounds_3[0], bounds_3[-1] # Dynamically populated by user input for --n_measurements
            non_mito_OCR_merged = calc_non_mito_OCR(df_perm, non_mito_OCR_first, non_mito_OCR_last) # Calculate non_mito_OCR by calling calc_non_mito_OCR function
            
            # Calculate substrate-dependent mOCR
            sub_dep_OCR_first, sub_dep_OCR_last = bounds_2[0], bounds_2[-1]
            sub_dep_out = substrate_or_tmpd_dependent_mOCR(non_mito_OCR_merged, sub_dep_OCR_first, sub_dep_OCR_last, "substrate")
            sub_dep_out_fin = sub_dep_out.copy()
            sub_dep_out_fin['Measurement'] = repeat # Filter on measurements relevant to specific repeat (as defined in final name). NOTE: Requires correct input file naming conventions.
            sub_dep_out_fin.rename(columns = {'Substrate_dependent_mOCR':'OCR'}, inplace = True) # Rename column to 'OCR' as per format required for input into R script 
            append_to_csv(sub_dep_out_fin, substrate_dep_out_file, count)

            # Calculate TMPD-dependent mOCR
            tmpd_dep_OCR_first, tmpd_dep_OCR_last = bounds_4[0], bounds_4[-1]
            tmpd_dep_out = substrate_or_tmpd_dependent_mOCR(non_mito_OCR_merged, tmpd_dep_OCR_first, tmpd_dep_OCR_last, "tmpd")
            tmpd_dep_out_fin = tmpd_dep_out.copy()
            tmpd_dep_out_fin['Measurement'] = repeat # Filter on measurements relevant to specific repeat (as defined in final name). NOTE: Requires correct input file naming conventions.
            tmpd_dep_out_fin.rename(columns = {'Tmpd_dependent_mOCR':'OCR'}, inplace = True) # Rename column to 'OCR' as per format required for input into R script 
            append_to_csv(tmpd_dep_out_fin, tmpd_dep_out_file, count)

            # Calculate ratio TMPD / Substrate mOCR 
            ratio_tmpd_substrate = ratio_TMPD_to_substrate(tmpd_dep_out, sub_dep_out, tmpd_dep_OCR_first, tmpd_dep_OCR_last) # takes in df's from tmpd_dep_out and sub_dep_out OCR as inputs
            ratio_tmpd_substrate_fin = ratio_tmpd_substrate.copy()
            ratio_tmpd_substrate_fin['Measurement'] = repeat # Filter on measurements relevant to specific repeat (as defined in final name). NOTE: Requires correct input file naming conventions.
            ratio_tmpd_substrate_fin = ratio_tmpd_substrate_fin[['Measurement', 'Well', 'Group', 'Days_on_Drug', 'Reading', 'OCR']] # Re-organise columns as per format required for input into R script
            append_to_csv(ratio_tmpd_substrate_fin, ratio_tmpd_out_file, count)

    # Process intermediate CSV files with R script 
    # R-script accepts an argument for directory of files to process 
    r_script_exe = get_rscript_path() # Automatically determine path to most recent version of Rscript.exe (Windows only) 
    R_script_output_dir, intact_data_output_dir, perm_data_output_dir = R_script_output_dir.resolve(), intact_data_output_dir.resolve(), perm_data_output_dir.resolve() # Resolve full path of directories, in event only relative paths provided for --input_dir/--output_dir
    R_script_output_dir, intact_data_output_dir, perm_data_output_dir = R_script_output_dir.as_posix(), intact_data_output_dir.as_posix(), perm_data_output_dir.as_posix() # Rscript.exe prefers posix paths 
    post_Rscript_path = (pathlib.Path(__file__).parent / "treat_seahorse_data.r").as_posix() # Get path of R_script to call with Python via subprocess(). NOTE: Assumes "treat_seahorse_data.r" in same path as "pre_R_script.py"
    print(f"Running R-script against intermediate data produced by Python script ... ")
    if args.intact: # Only perform if user specifies 'intact' option at commandline 
        subprocess.run([r_script_exe, post_Rscript_path, fr"--input_dir={intact_data_output_dir}", fr"--output_dir={R_script_output_dir}"], stdout=subprocess.PIPE) # Run R script via Python
    if args.perm: # Only perform if user specifies 'perm' option at commandline
        subprocess.run([r_script_exe, post_Rscript_path, fr"--input_dir={perm_data_output_dir}", fr"--output_dir={R_script_output_dir}"], stdout=subprocess.PIPE) # Run R script via Python
    
if __name__ == "__main__":
    main() # execute main function if script called directly (won't execute if functions are imported)


