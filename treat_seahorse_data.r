# Install the required modules, but only if not already installed 
pkgLoad <- function( packages = "favourites" ) {

    if( length( packages ) == 1L && packages == "favourites" ) {
        packages <- c( "lmerTest", "pbkrtest", "emmeans","ggplot2","ggsignif", 
        "argparse", "data.table"
        )
    }

    packagecheck <- match( packages, utils::installed.packages()[,1] )

    packagestoinstall <- packages[ is.na( packagecheck ) ]

    if( length( packagestoinstall ) > 0L ) {
        utils::install.packages( packagestoinstall,
                             repos = "http://cran.csiro.au"
        )
    } else {
        print( "All requested packages already installed" )
    }

    for( package in packages ) {
        suppressPackageStartupMessages(
            library( package, character.only = TRUE, quietly = TRUE )
        )
    }

}

pkgLoad() # Call the package installation function 

# Import the modules
library(argparse)
library(tools)
library(lmerTest)
library(pbkrtest)
library(emmeans)
library(data.table)
library(broom)

# Use argparse module to handle user arguments for input_dir and output_dir 
# This allows it to be programtically called by pre_R_script.py
# Can alternatively be invoked at command line to run across the intermediate output of pre_R_script.py
# i.e. where "<output_folder/temp_dir/csv_input_to_R_script" is specified as the --input_dir
parser <- ArgumentParser()
# specify our desired options 
# by default ArgumentParser will add an help option 
parser$add_argument("-i", "--input_dir",
    help="Full path to input directory of files to process")
parser$add_argument("-o", "--output_dir",
    help="Full path to output file for processed files")
args <- parser$parse_args()
# Store input argparse values in variables for use in script 
intermediate_dir <- args$input_dir
output_dir <- args$output_dir

# Jenni R script begins 
setwd(intermediate_dir)
file_list <- list.files() # list of files to process
temp = list.files(pattern="*.csv")

# Iterate over each input file in a loop 
for (f in temp){ 
    full_in_path = normalizePath(f) # full path to read in input CSV files
    full_out_path = file.path(output_dir, f) # full path to write processed CSV files
    seahorse=read.csv(f, sep=" ") # read in CSV for processing 
    seahorse$Measurement=factor(seahorse$Measurement)
    seahorse$Group=factor(seahorse$Group)
    seahorse$Well=factor(seahorse$Well)

    # Fit a linear mixed-effects model to the dataset
    model1=lmer(OCR~Group+(1|Measurement)+(1|Measurement:Well),data=seahorse)
    # model1=lmer(log(OCR)~Group+(1|Measurement)+(1|Measurement:Well),data=seahorse) # if you need to log transform the data
    anova(lm(OCR~Group+Days_on_Drug,data=seahorse))
    anova(model1)
    summary(model1)
    results=emmeans(model1,pairwise~Group)
    # results=emmeans(model1,pairwise~Group, type="response") #if you need to log transform the data

    # plot data
    summarisedinfo=tidy(emmeans::emmeans(model1,"Group"))
    
    # Write output to disk 
    write.table('SUMMARYINFO',full_out_path, append=FALSE, sep=" ", col.names=FALSE, row.names=FALSE, quote=FALSE) # heading. First entry to CSV so append=False
    write.table(print(summarisedinfo),full_out_path, append=TRUE, sep=" ",col.names=TRUE, row.names=FALSE, quote=TRUE) # append summary info to existing CSV
    write.table(',',full_out_path, append=TRUE, sep=" ", col.names=FALSE, row.names=FALSE, quote=FALSE) # add blank line
    write.table('EMMEANS',full_out_path, append=TRUE, sep=" ", col.names=FALSE, row.names=FALSE, quote=FALSE) # heading
    write.table(results[[1]],full_out_path, append=TRUE, sep=" ", col.names=TRUE, row.names=FALSE, quote=TRUE) # append results info to existing CSV
    write.table(',',full_out_path, append=TRUE, sep=" ", col.names=FALSE, row.names=FALSE, quote=FALSE) # add blank line
    write.table('CONTRASTS',full_out_path, append=TRUE, sep=" ", col.names=FALSE, row.names=FALSE, quote=FALSE) # heading
    write.table(results[[2]],full_out_path, append=TRUE, sep=" ", col.names=TRUE, row.names=FALSE, quote=TRUE) # append results info to existing CSV
    write.table(',',full_out_path, append=TRUE, sep=" ", col.names=FALSE, row.names=FALSE, quote=FALSE) # add blank line

}