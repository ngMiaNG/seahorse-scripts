## What is this repository for?
This repository hosts some scripts that are referenced in the paper "**Tools to investigate and dissect mitochondrial electron transport chain function in Toxoplasma gondii parasites using a Seahorse XFe96 extracellular flux analyzer**".
The scripts are stored here so that they can easily be shared and to easily manage any changes that may need to be made to the scripts at a later date.

---

## What do these scripts do?
There are two scripts: pre_R_script.py (Python script) and treat_seahorse_data.r (R script).
The pre_R_script.py cleans up the raw Seahorse input data (creates .csv copies of .xlsx input files for processing, renames some fields) and performs some
simple calculations (e.g. OCR, spare capacity etc.) that are required for the more complex calculations performed by the treat_seahorse_data.r 
script. 

**NOTE:** You ONLY need to run the pre_R_script.py script - it will automatically call the treat_seahorse_data.r scipt during execution.

---

## Getting started
To run these scripts you will require:

1. A local copy of this repository downloaded to a suitable location 
2. A Windows, Mac OS or Debian Linux operating system. The scripts have been tested on: Windows 10, Mac Sierra 10.12 and Linux Ubuntu  
3. Python3 (preferably >= 3.7)
4. Rscript.exe (i.e. have R installed)
5. An internet connection to install required non-standard Python3 and R modules
6. Seahorse assay data files that follow specific naming requirements (see SEAHORSE FILE NAMING REQUIREMENTS below)

---

## Installation
1. Clone or download this repository to a suitable location on your workstation, either by using the "git clone" functionality or the "download zip" functionality.
2. The Python script makes use of several non-standard modules. From a command prompt, pip install the requirements.txt file: `pip3 install -r requirements.txt`
### Additional help with pip installation of required modules:
For those unfamilar with the command line, there are various ways to do this. One of the easiest ways is to:

1. In File Explorer, navigate to the folder where you downloaded the repository (i.e. folder containing the scripts)
2. Go to Start > type cmd.exe to launch a standard command prompt
3. Click and drag the "requirements.txt" file from the folder to the command prompt (this will populate it with something like "<folder_path>/requirements.txt")
4. In the command prompt, move your cursor to the start of  "<folder_path>/requirements.txt" and paste in "pip install -r". This should result in `pip install -r <folder_path>/requirements.txt`. Hit enter

**NOTE:** If python isn't added to the PATH environmental variable (a check box on install), locate where your Python executable is and replace this command with: `<file_path>/python3.exe -m pip install -r requirements.txt`

**NOTE:** If this fails, you may need to manually install modules with the command `pip3 install <module>` from within the command prompt. This would have to be repeated for each line in the "requirements.txt" file.

**NOTE:** This only has to be done once (if installation of modules was successful)

---

## Usage
**NOTE:** The Python script needs to be invoked at the command-line via a terminal (e.g. CMD, PowerShell, Bash etc.) with specific arguments (arguments specify information like input, output etc. - e.g. --input is an "argument")

**NOTE:** The example commands here assume the commands are being rin from the Windows CMD shell. The script can be ran from any shell/interpreter (e.g. PowerShell, Bash) with appropriately modified commands. 

### Help
`python3 pre_R_script.py -h` to get built-in help

### Path to input directory
**NOTE:** Commands assume you are in the directory containing the scripts at the command-line. To change to this directory in the terminal, in File Explorer navigate to the folder containing the scripts 
(e.g. "C:\Users\User\Documents\seahorse-scripts") and drag it into the terminal. In the command prompt, move your cursor to the start of the line, type in `cd` with a space. This should look something like
this: `cd "C:\Users\User\Documents\seahorse-scripts"`. Hit enter.

The full file path to the directory containing raw Seahorse assay files (all repeats). These are typically in .xlsx files. For best results, these should be placed inside a folder by themselves without other files. It is often best to encapulate the file path in quotes. Make sure to use the proper slashes for your operating system in the file path (typically `\` for Windows and `/` for Linux). 

**EXAMPLE:** `python3 pre_R_script.py -i "C:\Users\User\Documents\directory_with_my_data"`

### Path to output directory
The full file path to the output directory to write results to. If the directory doesn't exist it will be created for you. It is often best to encapulate the file path in quotes. Make sure to use the proper slashes for your operating system (typically `\` for Windows and `/` for Linux).

**EXAMPLE:** `python3 .\pre_R_script.py -i "C:\Users\User\Documents\directory_with_my_data" -o "C:\Users\User\Documents\Seahorse_proccessed_results_dir"`

### Specify the number of measurements 
Specify the number of measurements to use for initial Seahorse calculations (i.e. non-mitochondrial OCR, basal/maximal OCR, basal ECAR, spare capacity calculations). E.g. For three measurements, type the number 3.

**EXAMPLE:** `python3 .\pre_R_script.py -i "C:\Users\User\Documents\directory_with_my_data" -o "C:\Users\User\Documents\Seahorse_proccessed_results_dir" -n 3`

### Specify the type of experiment performed
The user must specify which type of experiment was conducted. Supported types are:

1) Intact: intact parasites. Command-line option: --intact
2) Perm: permeabilized parasites. Command-line option: --perm

**EXAMPLE:** `python3 .\pre_R_script.py -i "C:\Users\User\Documents\directory_with_my_data" -o "C:\Users\User\Documents\Seahorse_proccessed_results_dir" -n 3 --intact`

**NOTE:** Change to `--perm` for perm condition
**NOTE:** Due to specifics in how the calculations are performed, a user can select ONE of `--intact` or `--perm`: not both. Passing both will raise an error and cause the script to exit.
**NOTE:** This should also align with the Seahorse file naming requirements (see "Seahorse file naming requirements" points 2 and 3)

---

## Seahorse file naming requirements 
1. Input .xlsx files should be named with format `rep<number>_<condition>.xlsx` 
Examples of good names: "rep1_intact.xlsx", "rep2_perm.xlsx". 
Examples of bad names: "intact_rep1.xlsx", "rep1.xlsx", "perm.xlsx"

	**NOTE:** If you want to run the script against existing Seahorse data without these naming conventions, this will require renaming the file names to match this convention


	**NOTE:** Do NOT include a space or underscore between "rep" and the number. See supplied test_data for example data. 


	**NOTE:** Script will attempt to identify when the incorrect file naming convention is used, and will alert the user before exiting.

2. For the 'intact' and 'perm' experimental conditions, the group name (the values in the 'Group' column of the Seahorse files) must start with the day on drug and end with the condition (either 'intact' or 'perm'), with a space between the number and day. For example: "0 day WT intact", "1 day rQCR11 intact" - see test_data for example data.

	**NOTE:** The script will attempt to identify when the incorrect Group naming convention is used, and will alert the user before exiting.

3. Group names need to be identical between all repeats. Otherwise they will be treated as seperate groups and not analysed together, resulting in incorrect output. For example: "0 day WT malate perm" must have this name consistently across all repeats.

---

## Output folders and files
The script outputs a few sets of output files into the folder specified with --output_dir:

1. The 'temp_dir' folder contains intermediate files used by the Python script to process the data into a format that can be used by the R script. You shouldn't need to use these files.
	1. 1.1. The 'xslx_2_csv_files' folder contains intermediate .csv files created from the input .xlsx files. These are otherwise unaltered (no filtering or additional calculations performed). These files are inputs into the Python script to perform initial calculations (e.g. basal OCR etc.).
	1. 1.2 The 'csv_input_to_R_script' folder contains intermediate .csv files containing the results of calculations (e.g. basal OCR etc.). These files are the input for the R script.
		* 1.2.1 The 'intact_data' folder contains the intermediate .csv files if the 'intact' option was used
		* 1.2.2 The 'perm_data' folder contains the intermediate .csv files if the 'perm' option was used
2. The 'R_script_output' directory contains the final output of the Seahorse R script. Contains CSV files with estimated marginal means, 95% confidence intervals and statistical analysis results
---

## Test data
Two representative "raw" Seahorse data sets have been included alongside the scripts. These can be found in ```<your_folder>/test_data```. The two sets are:

1. INTACT_TEST_DATA (intact parasites). This was performed with 5 measurements per well.
To produce representative processed output, run the script over this directory with the following arguments (note syntax will differ depending if done in Windows or Linux/MacOS shell):
```python3 pre_R_script.py -i "test_data\INTACT_TEST_DATA" -o "INTACT_ANALYSED" -n5 --intact```

2. PERM_TEST_DATA (permeabilized parasites). This was performed with 5 measurements per well.
To produce representative processed output, run the script over this directory with the following arguments (note syntax will differ depending if done in Windows or Linux/MacOS shell):
```python3 pre_R_script.py -i "test_data\perm_TEST_DATA" -o "PERM_ANALYSED" -n5 --perm```

This representative test data will also assist with troubleshooting (see "Troubleshooting" below).

---

## Troubleshooting
To help expedite troubleshooting, the script will attempt to ensure certain conditions required for the script to run correctly are met, and if they aren't, exits and prints a useful message to the terminal. These include:

1. The user specified neither --intact or --perm. Exits asking the user to supply one, not NONE
2. The user specified both --intact and --perm. Exits asking the user to supply ONE, not both
3. Checks if the input folder exists. If not, asks the user to double check if the correct folder was used and exits
4. Checks if the input folder exists but is empty. If empty, asks the user to double check if the correct folder was used and exits
5. For each input file name, checks if correct naming convention is used (`rep<num>_<condition>` - e.g. rep1_intact). If not, asks the user to double check and fix the name and exits
6. Checks if 'intact' or 'perm' in file name (e.g. rep1_perm) but the corresponding --intact or --perm was NOT supplied (e.g. rep1_perm used as file name but --intact was supplied at command-line). Warns user of mismatch and exits.
7. After filtering on only 'intact' or 'perm' data (depending on what the user specified), if there is NO intact or perm data, tells user and asks them to double check group naming conventions of input files, then exits.


Outside of these, prioritise troubleshooting based on the type of error received in command-line output. Some generic advice for troubleshooting various cateogries of errors is outlined below:

### File/directory related errors: 
**EXAMPLE:** `File/directory doesn't exist`. Double check for typos in the --input_dir, --output_dir arguments: typos, path not quoted, wrong slash type for OS etc.

### Module related errors: 
**EXAMPLE:** `Module doesn't exist`. Double check that required Python modules were installed via the requirements.txt file or manually with pip3 install <module>. 
R modules should be automatically downloaded and installed; if this fails, try manually installing.

### Script/data processing related errors: 
Harder to troubleshoot. Double check that your input Seahorse files follow the correct name conventions (see "SEAHORSE FILE NAMING REQUIREMENTS" above). 
Try running the script against the test data by specifying the "test_data" directory as the --input_dir. If script works against the test data, it is likely failing due to some attribute of your data. 
Compare your data with the test data to identify differences that may account for the errors.